// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

// ImageCreate to create an image
func (c *Client) ImageCreate(name string, size int64) (*Image, error) {
	r := ImageCreateRequest{
		Image: &Image{Name: name, Size: size},
	}

	reply, err := c.redctlClient.ImageCreate(c.Context(), &r)
	if err != nil {
		return nil, err
	}

	return reply.GetImage(), nil
}

// ImageCreate to create an image
func (c *Client) ImageCopy(source string, destination string) (*Image, error) {
	r := ImageCopyRequest{
		Source:      source,
		Destination: destination,
	}

	reply, err := c.redctlClient.ImageCopy(c.Context(), &r)
	if err != nil {
		return nil, err
	}

	return reply.GetImage(), nil
}

// ImageCreateBacked to create an image backed by another image
func (c *Client) ImageCreateBacked(name string, size int64, backingImageName string) (*Image, error) {
	r := ImageCreateBackedRequest{
		Image:        &Image{Name: name, Size: size},
		BackingImage: &Image{Name: backingImageName},
	}

	reply, err := c.redctlClient.ImageCreateBacked(c.Context(), &r)
	if err != nil {
		return nil, err
	}

	return reply.GetImage(), nil
}

// ImageFind to find an image
func (c *Client) ImageFind(name string) (*Image, error) {
	r := ImageFindRequest{
		Name: name,
	}

	d, err := c.redctlClient.ImageFind(c.Context(), &r)
	if err != nil {
		return nil, err
	}

	images := d.GetImages()
	if images == nil {
		return nil, nil
	}

	return d.GetImages()[0], nil
}

// ImageFindAll to find all images
func (c *Client) ImageFindAll() ([]*Image, error) {
	r := ImageFindRequest{}

	d, err := c.redctlClient.ImageFind(c.Context(), &r)
	if err != nil {
		return nil, err
	}

	return d.GetImages(), nil
}

// ImageRemove to remove an image
func (c *Client) ImageRemove(name string) error {
	r := ImageRemoveRequest{
		Name: name,
	}

	_, err := c.redctlClient.ImageRemove(c.Context(), &r)
	return err
}
