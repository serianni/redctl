// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

// GraphicImport imports a new graphic
func (c *Client) GraphicImport(graphic *Graphic) error {
	r := &GraphicImportRequest{
		Graphic: graphic,
	}
	_, err := c.redctlClient.GraphicImport(c.Context(), r)

	return err
}

// GraphicRemove imports a new graphic
func (c *Client) GraphicRemove(name string) error {
	r := &GraphicRemoveRequest{
		Name: name,
	}
	_, err := c.redctlClient.GraphicRemove(c.Context(), r)

	return err
}

// GraphicFind finds an existing graphic
func (c *Client) GraphicFind(name string) (string, error) {
	r := &GraphicFindRequest{
		Name: name,
	}
	reply, err := c.redctlClient.GraphicFind(c.Context(), r)
	if err != nil {
		return "", err
	}

	return reply.GetPath(), nil
}

// GraphicFindAll finds all graphics and returns their absolute paths.
func (c *Client) GraphicFindAll() ([]string, error) {
	r := &GraphicFindAllRequest{}

	reply, err := c.redctlClient.GraphicFindAll(c.Context(), r)
	if err != nil {
		return nil, err
	}

	return reply.GetPaths(), nil
}
