GOPATH ?= $(HOME)/go
PATH := $(GOPATH)/bin:$(PATH)
DESTDIR ?= /

.PHONY: all
all: bins

.PHONY: bins
bins:
	mkdir -p bin
	go build -o bin/redctl cmd/redctl/*.go
	go build -o bin/redctld cmd/redctld/*.go

.PHONY: install
install:
	install -d -m 0755 $(DESTDIR)/etc/bash_completion.d
	install -m 0755 configs/redctl.bash_completion.sh $(DESTDIR)/etc/bash_completion.d/

.PHONY: install-bins
install-bins: bins
	install -m 0755 bin/* $(DESTDIR)/usr/bin/

.PHONY: bats-tests
run-bats: bins
	sudo ./test/smoke.bats

.PHONY: clean
clean:
	rm -rf bin/

.PHONY: deps
deps:
	go get ./...


.PHONY: fmt
fmt:
	find api/ cmd/ internal/ test/ -name '*.go' | xargs gofmt -w -s
	find api/ cmd/ internal/ test/ -name '*.go' | xargs goimports -w

.PHONY: vendor
vendor:
	GO111MODULE=on go mod vendor

api/redctl.pb.go: api/redctl.proto
	protoc -I api --go_out=plugins=grpc:api api/redctl.proto

.PHONY: proto
proto: api/redctl.pb.go

.PHONY: check
check: golint all test
	DESTDIR=/tmp make install
	go install `go list -f  "{{.ImportPath}}" "{{.TestGoFiles}}" ./...`

.PHONY: test
test:
	go test -v ./...

.PHONY: golint
golint:
	golangci-lint --verbose run --enable-all -Dgochecknoglobals -Dgochecknoinits -Dlll
