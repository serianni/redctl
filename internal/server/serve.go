// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//go:generate protoc -I ../../api --go_out=plugins=grpc:../../api ../../api/api.proto

package server

import (
	"log"
	"net"
	"net/url"
	"os"

	"path/filepath"

	"github.com/pkg/errors"

	"gitlab.com/redfield/redctl/internal/server/repository"
)

func prepareSocketDirectory(path string) error {
	// remove socket if already exists (not always cleaned up on shutdown)
	os.Remove(path)

	// create directory if does not exist
	dir := filepath.Dir(path)
	return os.MkdirAll(dir, 0755)
}

func listen(opts *RedctlServerOptions) (net.Listener, error) {
	u, err := url.Parse(opts.ServerURL)
	if err != nil {
		return nil, errors.WithMessage(err, "failed to parse server url")
	}

	switch u.Scheme {
	case "unix":
		if err := prepareSocketDirectory(u.Path); err != nil {
			log.Println("Failed to create socket path, but will attempt listen anyway")
		}

		ln, err := net.Listen("unix", u.Path)
		if err == nil {
			log.Println("Listening on unix socket:", u.Path)
		}
		return ln, err
	case "http", "https", "tcp":
		ln, err := net.Listen("tcp", u.Host)
		if err == nil {
			log.Println("Listening on tcp:", u.Host)
		}
		return ln, err
	}
	return nil, errors.WithMessage(err, "invalid server url scheme")
}

// Serve backends for redctl
func Serve(opts RedctlServerOptions) error {
	if opts.TLSCertFile == "" {
		return errors.New("invalid argument")
	}

	if opts.TLSKeyFile == "" {
		return errors.New("invalid argument")
	}

	err := StorageInitialize(&opts)
	if err != nil {
		return errors.WithMessage(err, "failed to initialize storage")
	}

	db, err := ScribbleInitialize(opts.DbPath)
	if err != nil {
		return errors.WithMessage(err, "failed to initialize scribble")
	}

	domainRepo := repository.NewScribbleDomainRepo(db)
	imageRepo := repository.NewFilesystemImageRepo(opts.ImagesPath)
	xenDriver := &xenDriver{}
	graphicsRepo := repository.NewGraphicsRepo(opts.GraphicsPath)

	if err := graphicsRepo.ImportDirectory(opts.DataDir); err != nil {
		log.Printf("Failed to initialize graphics repo: %v", err)
	}

	ln, err := listen(&opts)
	if err != nil {
		log.Printf("failed to listen: %v", err)
		return err
	}
	defer ln.Close()

	return GrpcServe(ln, domainRepo, imageRepo, xenDriver, graphicsRepo, &opts)
}
