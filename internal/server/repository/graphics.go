// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package repository

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

const (
	FileExtSVG = ".svg"
	FileExtPNG = ".png"
)

// GraphicsRepo is used to manage graphics (e.g. desktop icons)
// on the filesystem
type GraphicsRepo struct {
	path string
}

// NewGraphicsRepo returns a new graphics repo with the
// given path
func NewGraphicsRepo(path string) *GraphicsRepo {
	return &GraphicsRepo{path}
}

func (gr *GraphicsRepo) graphicExists(name string) bool {
	path := gr.absPath(name)
	_, err := os.Stat(path)

	return !os.IsNotExist(err)
}

func (gr *GraphicsRepo) absPath(name string) string {
	return filepath.Join(gr.path, name)
}

// ImportDirectory imports all valid files (i.e. .png, .svg) from
// a given directory. In the case of a name conflict, old files
// are over-written by new ones.
func (gr *GraphicsRepo) ImportDirectory(path string) error {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}

	for _, file := range files {
		if !isValidGraphicsExtension(file.Name()) {
			continue
		}

		data, err := ioutil.ReadFile(filepath.Join(path, file.Name()))
		if err != nil {
			return err
		}

		err = gr.create(file.Name(), data, true)
		if err != nil {
			return err
		}
	}

	return nil
}

func isValidGraphicsExtension(name string) bool {
	ext := filepath.Ext(name)

	return (ext == FileExtPNG || ext == FileExtSVG)
}

func (gr *GraphicsRepo) create(name string, data []byte, overwrite bool) error {
	if gr.graphicExists(name) && !overwrite {
		return fmt.Errorf("graphic '%v' exists: overwrite not set", name)
	}

	return ioutil.WriteFile(gr.absPath(name), data, 0644)
}

// Imports adds a new graphic to the repo.
func (gr *GraphicsRepo) Import(name string, data []byte) error {
	if !isValidGraphicsExtension(name) {
		return fmt.Errorf("%v is not of format PNG or SVG", name)
	}

	return gr.create(name, data, false)
}

// Find returns a path to a graphic, if it exists. Otherwise an empty string
// is returned.
func (gr *GraphicsRepo) Find(name string) string {
	if !gr.graphicExists(name) {
		return ""
	}

	return gr.absPath(name)
}

// Remove removes the graphic from the repository
func (gr *GraphicsRepo) Remove(name string) error {
	if !gr.graphicExists(name) {
		return fmt.Errorf("graphic '%v' does not exist", name)
	}

	return os.Remove(gr.absPath(name))
}

// FindAll returns all the graphics in the repo.
func (gr *GraphicsRepo) FindAll() ([]string, error) {
	graphics := make([]string, 0)

	files, err := ioutil.ReadDir(gr.path)
	if err != nil {
		return graphics, fmt.Errorf("could not read files from %s: %v", gr.path, err)
	}

	for _, f := range files {
		if !isValidGraphicsExtension(f.Name()) {
			continue
		}

		graphics = append(graphics, gr.absPath(f.Name()))
	}

	return graphics, nil
}
